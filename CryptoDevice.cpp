#include "CryptoDevice.h"

using namespace std;
//aquire a default key for AES and block size for CBC. can't generate randomly, since all the agents shuold have the same key.
//can initialize with a unique sequence but not needed.
byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];


int key = 3;

string CryptoDevice::encryptAES(string plainText)
{
	string cipherText;
	for (int i = 0; plainText[i]; i++)
	{
		if (plainText[i] != ' ' && plainText[i] != '.' && plainText[i] != ',')
		{
			cipherText[i] = (plainText[i] - 'a' + key) % 26 + 'a';
		}
		else
		{
			cipherText[i] = plainText[i];
		}
	}
	return cipherText;
}

string CryptoDevice::decryptAES(string cipherText)
{
	string decryptedText;
	decryptedText = encryptAES(cipherText);
	return decryptedText;
}